const serverless = require('serverless-http');
const express = require('express')
const cheerio = require('cheerio');
const request = require('request');
const app = express()

const vgmUrl= 'https://www.digitalocean.com/community/tutorials/how-to-scrape-a-website-with-node-js';

app.get('/', function (req, res) {
  request(vgmUrl, function (error, response, html) {
    if (!error && response.statusCode == 200) {
      const $ = cheerio.load(html);
      let result = [];
      let title = $("meta[property='og:title']").attr("content");
      let description = $("meta[property='og:description']").attr("content");
      let image = $("meta[property='og:image']").attr("content");
  
      result.push({"title":title,"description":description,"image":image});
      res.send(result);
     /* res.send($("meta[property='og:title']").attr("content"));
      res.send($("meta[property='og:description']").attr("content"));
      res.send($("meta[property='og:image']").attr("content"));*/
      //or 
      //console.log($("meta").get(1).attr("content")); // index of the meta tag
    }
  });
})
module.exports.handler = serverless(app);